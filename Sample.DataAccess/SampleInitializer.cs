﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sample.Model;

namespace Sample.DataAccess
{
    public class SampleInitializer : DropCreateDatabaseIfModelChanges<SampleDataContext>
    {

        protected override void Seed(SampleDataContext context)
        {
            Console.WriteLine("Running Seed SampleInitializer");
            Debug.WriteLine("Running Seed method - debug");
            var parents = new List<Parent>
            {
                new Parent
                {
                    Name = "Jane Turner",
                    Children = new List<Child>()
                    {
                        new Child
                        {
                            Name = "Sophie Turner",
                            Chores = new List<Chore>
                            {
                                new Chore
                                {
                                    Description = "Clean Room",
                                    DueDateTime = DateTime.Parse("10 June 2015"),
                                    Urgent = false
                                },
                                new Chore
                                {
                                    Description = "Tidy Kitchen",
                                    DueDateTime = DateTime.Parse("11 June 2015"),
                                    Urgent = false
                                }
                            }
                        },

                        new Child
                        {
                            Name = "Harry Turner",
                            Chores = new List<Chore>
                            {
                                new Chore
                                {
                                    Description = "Clean bike",
                                    DueDateTime = DateTime.Parse("14 June 2015"),
                                    Urgent = false
                                },
                                new Chore
                                {
                                    Description = "Tidy Garage",
                                    DueDateTime = DateTime.Parse("15 June 2015"),
                                    Urgent = false
                                }
                            }
                        }



                    }

                },

                new Parent
                {
                    Name = "Tom Wilson",
                    Children = new List<Child>()
                    {
                        new Child
                        {
                            Name = "Ian Wilson",
                            Chores = new List<Chore>
                            {
                                new Chore
                                {
                                    Description = "Clean Room",
                                    DueDateTime = DateTime.Parse("10 June 2015"),
                                    Urgent = false
                                },
                                new Chore
                                {
                                    Description = "Tidy Kitchen",
                                    DueDateTime = DateTime.Parse("11 June 2015"),
                                    Urgent = false
                                }
                            }
                        },

                        new Child
                        {
                            Name = "David Wilson",
                            Chores = new List<Chore>
                            {
                                new Chore
                                {
                                    Description = "Clean bike",
                                    DueDateTime = DateTime.Parse("14 June 2015"),
                                    Urgent = false
                                },
                                new Chore
                                {
                                    Description = "Tidy Garage",
                                    DueDateTime = DateTime.Parse("15 June 2015"),
                                    Urgent = false
                                }
                            }
                        }

                    }
                }
            };

            parents.ForEach(s => context.Parents.Add(s));
            context.SaveChanges();
        }
    }
}
