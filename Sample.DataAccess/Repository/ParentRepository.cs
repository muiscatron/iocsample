﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sample.Model;

namespace Sample.DataAccess.Repository
{
    public class ParentRepository
    {

        private SampleDataContext _context;

        public ParentRepository(SampleDataContext context)
        {
            _context = context;
        }

        public List<Parent> GetAll()
        {
            return new List<Parent>(_context.Parents);
        }

    }
}
