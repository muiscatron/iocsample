﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sample.Model;

namespace Sample.DataAccess.Repository
{
    class ChildRepository
    {
        private SampleDataContext _context;

        public ChildRepository(SampleDataContext context)
        {
            _context = context;
        }

        public List<Child> GetAll()
        {
            return new List<Child>(_context.Children);
        }

    }
}
