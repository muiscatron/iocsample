﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Sample.Model;

namespace Sample.DataAccess.Repository
{
    public class ChoreRepository
    {
        private SampleDataContext _context;

        public ChoreRepository(SampleDataContext context)
        {
            _context = context;
        }

        public List<Chore> GetAll()
        {
            return new List<Chore>(_context.Chores);
        }

    }
}
