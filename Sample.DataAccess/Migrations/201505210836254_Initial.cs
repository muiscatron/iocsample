namespace Sample.DataAccess.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Child",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Parent_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Parent", t => t.Parent_Id)
                .Index(t => t.Parent_Id);
            
            CreateTable(
                "dbo.Chore",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Description = c.String(),
                        Urgent = c.Boolean(nullable: false),
                        DueDateTime = c.DateTime(nullable: false),
                        Child_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Child", t => t.Child_Id)
                .Index(t => t.Child_Id);
            
            CreateTable(
                "dbo.Parent",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Child", "Parent_Id", "dbo.Parent");
            DropForeignKey("dbo.Chore", "Child_Id", "dbo.Child");
            DropIndex("dbo.Chore", new[] { "Child_Id" });
            DropIndex("dbo.Child", new[] { "Parent_Id" });
            DropTable("dbo.Parent");
            DropTable("dbo.Chore");
            DropTable("dbo.Child");
        }
    }
}
