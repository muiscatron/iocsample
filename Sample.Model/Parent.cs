﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.Model
{
    public class Parent
    {
        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Child> Children { get; set; }

    }
}
