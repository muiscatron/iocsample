﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Sample.Model
{
    public class Chore
    {
        public int Id { get; set; }
        public string Description { get; set; }
        public bool Urgent { get; set; }
        public DateTime DueDateTime { get; set; }

    }
}
